

                           M A R T I A N   D E E P   3

                             Сopyright (С) 2013 - 2019, Vii.
							 
  Licensed under The MIT License https://tldrlegal.com/license/mit-license

  You have managed to activate the ancient space-time machine on Mars and
successfully transfered to Earth, 7119 B.C. in order to prevent Alice's death.
  Seek and destroy maritan base on Atlantis.

  Press "ENTER" when the game is loaded to start playing.

  Control keys:
  "W" or "up arrow" ........ move to the North
  "S" or "down arrow" ...... move to the South
  "A" or "left arrow" ...... move to the West
  "D" or "right arrow" ..... move to the East
  "Z" or "space" ........... jump
  "X" or "CTRL" ............ shoot
  "ESC" .................... menu

  Gameplay basics:
  You control the purple quad. Your parameters are listed in the bottom line
of the screen. The goal of the game is to destroy the martian base on Atlanis.
In order to do that, you must find and explode the underground torsion field
generator. Martians (the red quads) are attacking you. Try not letting them
close to you. Shoot at them. If a martian manages to come close to you, hit
him by pressing keys just like if you want to move towards the martian you want
to hit. Wathc your ammo counter! Collect ammo (blue quads) whenever possible.
  You premadie when your hitpoints are over. And Alice will permadie either,
because there is noone else to save her.

--------------------------------------------------------------------------------


                     М А Р С И А Н С К А Я   В П А Д И Н А   3

                             Сopyright (С) 2013 - 2019, Вий.

  Распространяется с лицензией MIT https://tldrlegal.com/license/mit-license

  Вы сумели активировать древнюю машину пространства-времени на Марсе и
перенеслись на землю в 7119 год до Н.Э., для того, чтобы предотвратить смерть
Алисы.
  Теперь Вам предстоит найти и уничтожить Марсианскую базу в Атлантиде.

  После загрузки программы нажмите клавишу "ENTER", чтобы начать играть.

  Управляющие клавиши:
  "W" или "стрелка вверх" ..... двигаться на север
  "S" или "стрелка вниз" ...... двигаться на юг
  "A" или "стрелка влево" ..... двигаться на запад
  "D" или "стрелка вправо" .... двигаться на восток
  "Z" или "пробел" ............ прыгнуть
  "X" или "CTRL" .............. выстрелить из пистолета
  "ESC" ....................... открыть меню

  Основы геймплея:
  Вы - фиолетовое знакоместо. Ваши параметры приведены в нижней строке экрана.
Цель игры - уничтожить Марсианскую базу в Атлантиде. Для этого Вы должны найти
и взорвать подземный генератор торсионного поля. Марсиане, вот ведь странные
создания, хотят вам помешать и атакуют со всех сторон (красные знакоместа).
Старайтесь не давать марсианам подойти к вам и ударить вас, стреляйте в них из
пистолета. Если марсианину все же удастся подойти вплотную, то бейте его (для
того, чтобы ударить марсианина, нажмите кнопки так, как будто вы пытаетесь
двигаться в его сторону). Следите за количеством патронов, пополняйте боезапас,
собирая ящики с патронами (голубое знакоместо).
  Если ваши хитпоинты закончатся, Вы перманентно умрете и так и не спасете
Алису, которая, соответственно, тоже перманентно умрет.

--------------------------------------------------------------------------------
  
  Music (c) by Andreas Viklund

  Track_01 - "Electronic Dreams" (Bjoern/P.TMstberg)
  Track_02 - "Faraway Love"
  Track_03 - "Fable"
  Track_04 - "Njals saga" by Andread & JoJo of TSEC
  Track_05 - "Solar Wind" [Original: DJ Dado]
  Track_06 - "Lonely heart"
  Track_07 - "A moment of love"

  TESC:
  Andread Viklund
  Bj"rn Karlsson
  John Johansson

  (C) TSEC
  
--------------------------------------------------------------------------------

  Sounds by artisticdude http://opengameart.org/                                                                                

--------------------------------------------------------------------------------

  Sounds (c) by Michel Baradari apollo-music.de

Licensed under CC BY 3.0 http://creativecommons.org/licenses/by/3.0/

Hosted on opengameart.org   

--------------------------------------------------------------------------------

Arctic Engine

Licensed under The MIT License https://tldrlegal.com/license/mit-license

The MIT License (MIT)

Copyright (c) 2016 - 2018 Huldra
Copyright (c) 2017 Romain Sylvian
Copyright (c) 2013 - 2017 Martin Mitáš
Copyright (c) 2015 - 2016 Inigo Quilez
Copyright (c) 2009 - 2010 Barry Schwartz
Copyright (c) 2018 Vitaliy Manushkin <agri@akamo.info>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


--------------------------------------------------------------------------------
