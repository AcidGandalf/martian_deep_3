// Martian Deep 3 (c) Vii 2013-2019
// Licensed under The MIT License

#pragma region Includes

#include <unordered_map>
#include "engine/easy.h"
#include "engine/unicode.h"


using namespace arctic;  // NOLINT

#include <math.h>

#pragma endregion


std::vector<std::string> vec = {
  u8"?", u8"☺", u8"☻", u8"♥", u8"♦", u8"♣", u8"♠", u8"•", u8"◘", u8"○", u8"◙", u8"♂", u8"♀", u8"♪", u8"♫", u8"☼",
  u8"►", u8"◄", u8"↕", u8"‼", u8"¶", u8"§", u8"▬", u8"↨", u8"↑", u8"↓", u8"→", u8"←", u8"∟", u8"↔", u8"▲", u8"▼",
  u8" ", u8"!", u8"\"", u8"#", u8"$", u8"%", u8"&", u8"'", u8"(", u8")", u8"*", u8"+", u8",", u8"-", u8".", u8"/",
  u8"0", u8"1", u8"2", u8"3", u8"4", u8"5", u8"6", u8"7", u8"8", u8"9", u8":", u8";", u8"<", u8"=", u8">", u8"?",
  u8"@", u8"A", u8"B", u8"C", u8"D", u8"E", u8"F", u8"G", u8"H", u8"I", u8"J", u8"K", u8"L", u8"M", u8"N", u8"O",
  u8"P", u8"Q", u8"R", u8"S", u8"T", u8"U", u8"V", u8"W", u8"X", u8"Y", u8"Z", u8"[", u8"\\", u8"]", u8"^", u8"_",
  u8"`", u8"a", u8"b", u8"c", u8"d", u8"e", u8"f", u8"g", u8"h", u8"i", u8"j", u8"k", u8"l", u8"m", u8"n", u8"o",
  u8"p", u8"q", u8"r", u8"s", u8"t", u8"u", u8"v", u8"w", u8"x", u8"y", u8"z", u8"{", u8"|", u8"}", u8"~", u8"⌂",
  u8"А", u8"Б", u8"В", u8"Г", u8"Д", u8"Е", u8"Ж", u8"З", u8"И", u8"Й", u8"К", u8"Л", u8"М", u8"Н", u8"О", u8"П",
  u8"Р", u8"С", u8"Т", u8"У", u8"Ф", u8"Х", u8"Ц", u8"Ч", u8"Ш", u8"Щ", u8"Ъ", u8"Ы", u8"Ь", u8"Э", u8"Ю", u8"Я",
  u8"а", u8"б", u8"в", u8"г", u8"д", u8"е", u8"ж", u8"з", u8"и", u8"й", u8"к", u8"л", u8"м", u8"н", u8"о", u8"п",
  u8"░", u8"▒", u8"▓", u8"│", u8"┤", u8"╡", u8"╢", u8"╖", u8"╕", u8"╣", u8"║", u8"╗", u8"╝", u8"╜", u8"╛", u8"┐",
  u8"└", u8"┴", u8"┬", u8"├", u8"─", u8"┼", u8"╞", u8"╟", u8"╚", u8"╔", u8"╩", u8"╦", u8"╠", u8"═", u8"╬", u8"╧",
  u8"╨", u8"╤", u8"╥", u8"╙", u8"╘", u8"╒", u8"╓", u8"╫", u8"╪", u8"┘", u8"┌", u8"█", u8"▄", u8"▌", u8"▐", u8"▀",
  u8"р", u8"с", u8"т", u8"у", u8"ф", u8"х", u8"ц", u8"ч", u8"ш", u8"щ", u8"ъ", u8"ы", u8"ь", u8"э", u8"ю", u8"я",
  u8"Ё", u8"ё", u8"Є", u8"є", u8"Ї", u8"ї", u8"Ў", u8"ў", u8"°", u8"∙", u8"·", u8"√", u8"№", u8"¤", u8"■", u8" "};

struct Symbol {
  Ui32 code;
  Rgba fore_color;
  Rgba back_color;
};

Symbol text_buf[80 * 25];
std::unordered_map<char32_t, Sprite> g_ch;
std::vector<char32_t> g_ascii;

Si32 text_x = 0;
Si32 text_y = 0;
Rgba text_fore_color = Rgba((Ui32)0xffffffff);
Rgba text_back_color = Rgba((Ui32)0x000000ff);

void cls() {
  for (Si32 i = 0; i < 80 * 25; ++i) {
    Symbol &s = text_buf[i];
    s.code = (Ui32)' ';
    s.fore_color.rgba = 0xffffffff;
    s.back_color.rgba = 0;
  }
  text_fore_color.rgba = 0xffffffff;
  text_back_color.rgba = 0;
  text_x = 0;
  text_y = 0;
}

Rgba decode_color(Ui32 color_code) {
  Ui32 bright = (color_code & 0x08) ? 128 : 0;
  Ui32 r = (color_code & 0x04) ? 127 : 0;
  Ui32 g = (color_code & 0x02) ? 127 : 0;
  Ui32 b = (color_code & 0x01) ? 127 : 0;
  return Rgba(bright + r, bright + g, bright + b, 255);
}
// control character is 0x25d1 or u8"◑"
const char * print(const char *text) {
  Utf32Reader reader;
  reader.Reset((const Ui8 *)text);
  for (Ui32 ch = reader.ReadOne(); ch != 0; ch = reader.ReadOne()) {
    if (ch == 0x25d1) {
      Ui32 color_code = reader.ReadOne();
      text_fore_color = decode_color(color_code);
    } else if (ch == '\n') {
      text_x = 0;
      text_y++;
    } else {
      Si32 pos = (text_y * 80 + text_x) % (80 * 25);
      Symbol &s = text_buf[pos];
      s.code = ch;
      s.fore_color = text_fore_color;
      s.back_color = text_back_color;
      text_x++;
    }
  }
  return (const char *)reader.p;
}

void present() {
  Clear();
  Sprite backbuffer = GetEngine()->GetBackbuffer();
  Sprite full = g_ch[0x2588]; //u8"█"

  for (Si32 i = 0; i < 80 * 25; ++i) {
    Symbol &s = text_buf[i];
    Si32 x = i % 80 * 8;
    Si32 y = ScreenSize().y - i / 80 * 16 - 16;
    full.Draw(backbuffer,
      x, y, kDrawBlendingModeColorize, kFilterNearest, s.back_color);
    g_ch[s.code].Draw(backbuffer,
      x, y, kDrawBlendingModeColorize, kFilterNearest, s.fore_color);
  }
  ShowFrame();
}


#pragma region Preprocessor definitions
// DEFINITIONS

#define SOUND_DIR "data"
#define SOUND_EXT "ogg"

#define MAX_CHARACTERS 64000

#define MAP_SIZE_BITS 9
#define MAP_SIZE (1<<MAP_SIZE_BITS)
#define MAP_SIZE_HALF (MAP_SIZE/2)
#define MAP_SIZE_HALF_1 (MAP_SIZE_HALF-1)
#define MAP_SIZE_1 (MAP_SIZE-1)
#define MAP_SIZE_MASK (uint32_t(MAP_SIZE-1))


#define MAP_HORIZON_Y 65

#define CELL_EMPTY 0
#define CELL_STONE 1
#define CELL_GRASS 2
#define CELL_EARTH 3
#define CELL_MAX 4

#define CHARACTER_PLAYER 0
#define CHARACTER_BODY 1
#define CHARACTER_BULLET 2
#define CHARACTER_MARTIANIAN 3
#define CHARACTER_AMMO 4
#define CHARACTER_DELETED 5

#define GRAVITY_TICKS 3
#define MOVEMENT_TICKS 1
#define SHOOTING_TICKS 2
#define MARTIANIAN_TICKS 4
#define JUMP_UP_TICKS 1

#define MARTIANIAN_COUNT 20000
#define AMMO_COUNT 4000

#pragma endregion

#pragma region Structure typedefs

// TYPES
struct IPoint3d {
  union {
    struct {
      int x;
      int y;
      int z;
    };
    int component[3];
  };

  IPoint3d() {
    x = 0;
    y = 0;
    z = 0;
  };

  IPoint3d(int x_, int y_, int z_) {
    x = x_;
    y = y_;
    z = z_;
  }

  void Set(int x_, int y_, int z_) {
    x = x_;
    y = y_;
    z = z_;
  }
};

IPoint3d operator+ (const IPoint3d &a, const IPoint3d &b) {
  IPoint3d result(a.x + b.x, a.y + b.y, a.z + b.z);
  return result;
}

IPoint3d operator- (const IPoint3d &a,const  IPoint3d &b) {
  IPoint3d result(a.x - b.x, a.y - b.y, a.z - b.z);
  return result;
}

IPoint3d operator* (const IPoint3d &a, int b) {
  IPoint3d result(a.x*b, a.y*b, a.z*b);
  return result;
}

IPoint3d operator/ (const IPoint3d &a, int b) {
  IPoint3d result(a.x / b, a.y / b, a.z / b);
  return result;
}

struct MapPos {
  int index;

  int getX() {
    return index & MAP_SIZE_MASK;
  }
  int getY() {
    return (index >> MAP_SIZE_BITS) & MAP_SIZE_MASK;
  }
  int getZ() {
    return (index >> (MAP_SIZE_BITS * 2)) & MAP_SIZE_MASK;
  }
  IPoint3d getPos() {
    return IPoint3d(index & MAP_SIZE_MASK, (index >> MAP_SIZE_BITS) & MAP_SIZE_MASK, (index >> (MAP_SIZE_BITS * 2)) & MAP_SIZE_MASK);
  }

  MapPos() {
    index = 0;
  }
  MapPos(int x, int y, int z) {
    index = ((z & MAP_SIZE_MASK) << (MAP_SIZE_BITS * 2)) | ((y & MAP_SIZE_MASK) << MAP_SIZE_BITS) | (x & MAP_SIZE_MASK);
  }
  MapPos(IPoint3d &pos) {
    index = ((pos.z & MAP_SIZE_MASK) << (MAP_SIZE_BITS * 2)) | ((pos.y & MAP_SIZE_MASK) << MAP_SIZE_BITS) | (pos.x & MAP_SIZE_MASK);
  }
  void setXyz(int x, int y, int z) {
    index = (z << (MAP_SIZE_BITS * 2)) | (y << MAP_SIZE_BITS) | x;
  }
  void setXyz(IPoint3d &pos) {
    index = (pos.z << (MAP_SIZE_BITS * 2)) | (pos.y << MAP_SIZE_BITS) | pos.x;
  }
  void setXyzSafe(int x, int y, int z) {
    index = ((z & MAP_SIZE_MASK) << (MAP_SIZE_BITS * 2)) | ((y & MAP_SIZE_MASK) << MAP_SIZE_BITS) | (x & MAP_SIZE_MASK);
  }
  void setXyzSafe(IPoint3d &pos) {
    index = ((pos.z & MAP_SIZE_MASK) << (MAP_SIZE_BITS * 2)) | ((pos.y & MAP_SIZE_MASK) << MAP_SIZE_BITS) | (pos.x & MAP_SIZE_MASK);
  }
};

struct Character {
  IPoint3d pos;
  IPoint3d controlDirection;
  IPoint3d lastControlDirection;
  int jumpUpLeft;
  int type;
  int ticksToGravity;
  int hp;
  int timeToLive;
  int timeToMove;
  int timeToShoot;
  int ammo;
  bool isShooting;
  bool isPlayerBullet;

  void Initialize() {
    pos.Set(0, MAP_HORIZON_Y, 0);
    controlDirection.Set(0, 0, 0);
    lastControlDirection.Set(1, 0, 0);
    jumpUpLeft = 0;
    type = CHARACTER_BODY;
    ticksToGravity = 0;
    hp = 100;
    timeToLive = 255;
    timeToMove = 0;
    timeToShoot = 0;
    isShooting = false;
    ammo = 20;
    isPlayerBullet = true;
  }
};

#pragma endregion

#pragma region Global variables
// VARIABLES

double prevTime;
double curTime;
float timeLapsed;

int frags;

int scrWidth;
int scrHeight;

int scrWidth_1;
int scrHeight_1;

int nCharacters;
Character* characters = NULL;

Character* pLocalPlayer = NULL;

Ui8 *map = NULL;
int *mapCharacters = NULL;

MapPos cameraPos;

bool isInTunnelingMode = false;

Sound sound_click;
Sound sound_menu_choise;

Sound sound_pick_item;
Sound sound_reload_weapon;
Sound sound_impact_wood;


const int sound_hit_count = 37;
Sound sound_hit[sound_hit_count];
const int sound_pain_count = 6;
Sound sound_pain[sound_pain_count];
const int sound_pistol_count = 5;
Sound sound_pistol[sound_pistol_count];
const int sound_monster_count = 18;
Sound sound_monster[sound_monster_count];
const int sound_jump_count = 4;
Sound sound_jump[sound_jump_count];

int currentMusicTrackIndex = 0;
Sound music;

int *neighbors2d = NULL;

#pragma endregion
#pragma region All other functions


void UpdateMusicPlayer() {
  if (music.IsPlaying()) {
    return;
  }
  char path[4096];
  const int NUMBER_OF_TRACKS = 7;
  currentMusicTrackIndex = (currentMusicTrackIndex + Random(0, NUMBER_OF_TRACKS - 2) + 1) % NUMBER_OF_TRACKS;
  sprintf(path, "data/track_%02d.ogg", currentMusicTrackIndex + 1);
  music.Load(path, false);
  music.Play();
}

void InitNeighbors2d() {
  neighbors2d = (int*)malloc(MAP_SIZE * MAP_SIZE * sizeof(int) * 8);
  for (int z = 0; z < MAP_SIZE; ++z) {
    for (int x = 0; x < MAP_SIZE; ++x) {
      int i = 0;
      for (int za = 0; za < 3; ++za) {
        for (int xa = 0; xa < 3; ++xa) {
          if (!(xa == 1 && za == 1)) {
            neighbors2d[i + 8 * (((x + 1)&MAP_SIZE_MASK) + MAP_SIZE*((z + 1)&MAP_SIZE_MASK))] = ((x + xa)&MAP_SIZE_MASK) + MAP_SIZE*((z + za)&MAP_SIZE_MASK);
            i++;
          }
        }
      }
    }
  }
}

void IterateCellAutomatonGeneration(int iterationsCount, char* &buffer, char* &buffer2) {
  for (int iteration = 0; iteration < iterationsCount; ++iteration) {
    for (int z = MAP_SIZE; z < MAP_SIZE * 2; ++z)
      for (int x = MAP_SIZE; x < MAP_SIZE * 2; ++x) {
        int trueCount = 0;
        for (int dz = -1; dz < 2; ++dz)
          for (int dx = -1; dx < 2; ++dx) {
            if (buffer[((x + dx)&MAP_SIZE_MASK) + ((z + dz)&MAP_SIZE_MASK)*MAP_SIZE])
              trueCount++;
          }
        buffer2[(x&MAP_SIZE_MASK) + (z&MAP_SIZE_MASK)*MAP_SIZE] = (trueCount >= 5);
      }
    char* exchange = buffer2;
    buffer2 = buffer;
    buffer = exchange;
  }
}

void PlantTrees(char* groundBuffer) {
  int treesToTryToPlant = MAP_SIZE * MAP_SIZE / 5;
  for (int treeId = 0; treeId < treesToTryToPlant; ++treeId) {
    int x = (int)Random(0, MAP_SIZE_1);
    int z = (int)Random(0, MAP_SIZE_1);
    if (groundBuffer[x + z * MAP_SIZE]) {
      int comaRadius = (int)Random(1, 3);
      int comaRadiusSq = comaRadius * comaRadius + 1;
      int trunkHeight = (int)Random(2, 4);

      for (int dz = -comaRadius; dz <= comaRadius; ++dz) {
        for (int dy = -comaRadius; dy <= comaRadius; ++dy) {
          for (int dx = -comaRadius; dx <= comaRadius; ++dx) {
            int radiusSq = dx*dx + dy*dy + dz*dz;
            if (radiusSq <= comaRadiusSq) {
              map[MapPos(x + dx, MAP_HORIZON_Y + trunkHeight + comaRadius + dy, z + dz).index] = CELL_GRASS;
            }
          }
        }
      }
      for (int dy = -1; dy < trunkHeight; ++dy) {
        map[MapPos(x, MAP_HORIZON_Y + dy, z).index] = CELL_EARTH;
      }

      int emptyZoneRadius = std::min(3, comaRadius + 1);
      int emptyZoneRadiusSq = emptyZoneRadius * emptyZoneRadius;
      for (int dz = -emptyZoneRadius; dz <= emptyZoneRadius; ++dz) {
        for (int dx = -emptyZoneRadius; dx <= emptyZoneRadius; ++dx) {
          int radiusSq = dx * dx + dz * dz;
          if (radiusSq <= emptyZoneRadiusSq) {
            groundBuffer[((x + dx)&MAP_SIZE_MASK)
              + ((z + dz) & MAP_SIZE_MASK) * MAP_SIZE] = 0;
          }
        }
      }
    }
  }
}

void PlantBushes(char* groundBuffer) {
  // 1 in groundBuffer - place to plant a bush
  int treesToTryToPlant = MAP_SIZE * MAP_SIZE / 5;
  for (int treeId = 0; treeId < treesToTryToPlant; ++treeId) {
    int x = (int)Random(0, MAP_SIZE_1);
    int z = (int)Random(0, MAP_SIZE_1);
    if (groundBuffer[x + z * MAP_SIZE]) {
      int comaRadius = (int)Random(1, 3);
      int comaRadiusSq = comaRadius * comaRadius + 1;

      for (int dz = -comaRadius; dz <= comaRadius; ++dz) {
        for (int dy = 0; dy <= comaRadius; ++dy) {
          for (int dx = -comaRadius; dx <= comaRadius; ++dx) {
            int radiusSq = dx*dx + dy*dy + dz*dz;
            if (radiusSq <= comaRadiusSq) {
              map[MapPos(x + dx, MAP_HORIZON_Y + dy, z + dz).index] = CELL_GRASS;
            }
          }
        }
      }

    }
  }
}


void InitSound() {
  char path[1024];

  sound_click.Load(SOUND_DIR"/click." SOUND_EXT, true);
  sound_menu_choise.Load(SOUND_DIR"/menu_choise." SOUND_EXT, true);

  sound_pick_item.Load(SOUND_DIR"/pick_item." SOUND_EXT, true);
  sound_reload_weapon.Load(SOUND_DIR"/reload_weapon." SOUND_EXT, true);
  sound_impact_wood.Load(SOUND_DIR"/impact_wood." SOUND_EXT, true);

  for (int i = 0; i < sound_hit_count; ++i) {
    sprintf(path, SOUND_DIR"/hit%02d." SOUND_EXT, i + 1);
    sound_hit[i].Load(path, true);
  }
  for (int i = 0; i < sound_pain_count; ++i) {
    sprintf(path, SOUND_DIR"/pain%d." SOUND_EXT, i + 1);
    sound_pain[i].Load(path, true);
  }
  for (int i = 0; i < sound_pistol_count; ++i) {
    sprintf(path, SOUND_DIR"/pistol%d." SOUND_EXT, i + 1);
    sound_pistol[i].Load(path, true);
  }
  for (int i = 0; i < sound_monster_count; ++i) {
    sprintf(path, SOUND_DIR"/monster-%d." SOUND_EXT, i + 1);
    sound_monster[i].Load(path, true);
  }
  for (int i = 0; i < sound_jump_count; ++i) {
    sprintf(path, SOUND_DIR"/jump%02d." SOUND_EXT, i + 1);
    sound_jump[i].Load(path, true);
  }

  UpdateMusicPlayer();
}


#define BACKGROUND_INTENSITY 0x80
#define BACKGROUND_RED 0x40
#define BACKGROUND_GREEN 0x20
#define BACKGROUND_BLUE 0x10

#define COL_0000 0
#define COL_1000 BACKGROUND_RED
#define COL_0100 BACKGROUND_GREEN
#define COL_1100 (BACKGROUND_RED | BACKGROUND_GREEN)
#define COL_0010 BACKGROUND_BLUE
#define COL_1010 (BACKGROUND_RED | BACKGROUND_BLUE)
#define COL_0110 (BACKGROUND_GREEN | BACKGROUND_BLUE)
#define COL_1110 (BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE)

#define COL_0001 BACKGROUND_INTENSITY
#define COL_1001 (BACKGROUND_RED | BACKGROUND_INTENSITY)
#define COL_0101 (BACKGROUND_GREEN | BACKGROUND_INTENSITY)
#define COL_1101 (BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_INTENSITY)
#define COL_0011 (BACKGROUND_BLUE | BACKGROUND_INTENSITY)
#define COL_1011 (BACKGROUND_RED | BACKGROUND_BLUE | BACKGROUND_INTENSITY)
#define COL_0111 (BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY)
#define COL_1101 (BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_INTENSITY)
#define COL_1111 (BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY)

void DrawSquare(int x, int y, int color) {
  Si32 pos = (y * 80 + x) % (80 * 25);
  Symbol &s = text_buf[pos];
  s.code = ' ';
  s.fore_color = decode_color(color);
  s.back_color = decode_color(color >> 4);
}

void DrawString(int x, int y, const char* psz, int forecolor, int backcolor) {
  if (y >= 0 && y < scrHeight) {
    for (int x1 = 0; x1<(int)strlen(psz); x1++) {
      if (x + x1 >= 0 && x + x1 < scrWidth) {
        Symbol &s = text_buf[x + x1 + y*scrWidth];
        s.code = psz[x1];
        s.fore_color = decode_color(forecolor >> 4);
        s.back_color = decode_color(backcolor);
      }
    }
  }
}

void ClearMapCharacters() {
  for (int i = 0; i < MAP_SIZE * MAP_SIZE * MAP_SIZE; ++i) {
    mapCharacters[i] = -1;
  }
}

void RemoveMapCharacters() {
  for (int i = 0; i < nCharacters; ++i) {
    Character &ch = characters[i];
    mapCharacters[MapPos(ch.pos).index] = -1;
  }
}

void FillMapCharacters() {
  for (int i = 0; i < nCharacters; ++i) {
    Character &ch = characters[i];
    mapCharacters[MapPos(ch.pos).index] = i;
  }
}

void MoveCharacter(int n, IPoint3d &toPos) {
  mapCharacters[MapPos(characters[n].pos).index] = -1;
  mapCharacters[MapPos(toPos).index] = n;
  characters[n].pos = MapPos(toPos).getPos();
}

void DeleteCharacter(int n) {
  mapCharacters[MapPos(characters[n].pos).index] = -1;
  characters[n].type = CHARACTER_DELETED;
}

void WipeDeletedCharacter(int n) {
  nCharacters--;
  characters[n] = characters[nCharacters];
  if (n < nCharacters && mapCharacters[MapPos(characters[n].pos).index] == nCharacters)
    mapCharacters[MapPos(characters[n].pos).index] = n;
}

void AddCharacter(int n) {
  mapCharacters[MapPos(characters[n].pos).index] = n;
}

void PlayerInit() {
  frags = 0;

  cameraPos.setXyz(MAP_SIZE_HALF, MAP_SIZE_HALF, MAP_SIZE_HALF);
}

void SetResolution(int x, int y) {
  scrWidth = x;
  scrHeight = y;
  scrWidth_1 = scrWidth - 1;
  scrHeight_1 = scrHeight - 1;

  cls();
  present();
}

void HardInit() {
  SetResolution(80, 25);
  InitSound();
}

void InitMap() {
  if (map) {
    free(map);
    map = NULL;
  }

  if (mapCharacters) {
    free(mapCharacters);
    mapCharacters = NULL;
  }

  if (!map) {
    map = (Ui8*)malloc(MAP_SIZE * MAP_SIZE * MAP_SIZE);

    for (int z = 0; z < MAP_SIZE; ++z)
      for (int x = 0; x < MAP_SIZE; ++x)
        map[x + z * MAP_SIZE*MAP_SIZE] = CELL_STONE;

    for (int y = 1; y < MAP_HORIZON_Y; ++y)
      for (int z = 0; z < MAP_SIZE; ++z)
        for (int x = 0; x < MAP_SIZE; ++x)
          map[x + y*MAP_SIZE + z * MAP_SIZE*MAP_SIZE] = CELL_EARTH;

    for (int z = 0; z < MAP_SIZE; ++z)
      for (int x = 0; x < MAP_SIZE; ++x)
        map[x + MAP_HORIZON_Y*MAP_SIZE + z * MAP_SIZE*MAP_SIZE] = CELL_GRASS;

    for (int y = MAP_HORIZON_Y + 1; y < MAP_SIZE; ++y)
      for (int z = 0; z < MAP_SIZE; ++z)
        for (int x = 0; x < MAP_SIZE; ++x)
          map[x + y*MAP_SIZE + z * MAP_SIZE*MAP_SIZE] = CELL_EMPTY;

    char *treesBuffer = (char*)malloc(MAP_SIZE * MAP_SIZE);
    char *bushesBuffer = (char*)malloc(MAP_SIZE * MAP_SIZE);
    char *tempBuffer = (char*)malloc(MAP_SIZE * MAP_SIZE);
    char *caveBuffer = (char*)malloc(MAP_SIZE * MAP_SIZE);
    // plant trees
    uint32_t treesThreshold = 50;
    uint32_t bushesThreshold = 40;

    for (Si32 i = 0; i < MAP_SIZE * MAP_SIZE; ++i) {
      ((uint8_t*)(void*)treesBuffer)[i] = (Ui8)Random(0, 255);
    }
    for (int i = 0; i < MAP_SIZE * MAP_SIZE; ++i) {
      if (uint32_t(treesBuffer[i]) % 100 > treesThreshold)
        treesBuffer[i] = 0;
    }
    for (Si32 i = 0; i < MAP_SIZE * MAP_SIZE; ++i) {
      ((uint8_t*)(void*)bushesBuffer)[i] = (Ui8)Random(0, 255);
    }
    for (int i = 0; i < MAP_SIZE * MAP_SIZE; ++i) {
      if (uint32_t(bushesBuffer[i]) % 100 > bushesThreshold)
        bushesBuffer[i] = 0;
    }

    IterateCellAutomatonGeneration(6, bushesBuffer, tempBuffer);
    PlantBushes(bushesBuffer);

    IterateCellAutomatonGeneration(6, treesBuffer, tempBuffer);
    PlantTrees(treesBuffer);

    for (int y = 0; y > -MAP_HORIZON_Y; y--) {
      uint32_t caveThreshold = abs(y) % 2 ? 50 : 25;

      for (Si32 i = 0; i < MAP_SIZE * MAP_SIZE; ++i) {
        ((uint8_t*)(void*)caveBuffer)[i] = (Ui8)Random(0, 255);
      }
      for (int i = 0; i < MAP_SIZE * MAP_SIZE; ++i) {
        if (uint32_t(caveBuffer[i]) % 100 > caveThreshold)
          caveBuffer[i] = 0;
      }
      IterateCellAutomatonGeneration(6, caveBuffer, tempBuffer);
      for (int z = 0; z < MAP_SIZE; ++z) {
        for (int x = 0; x < MAP_SIZE; ++x) {
          if (caveBuffer[x + z * MAP_SIZE]) {
            map[MapPos(x, y + MAP_HORIZON_Y, z).index] = CELL_EMPTY;
          }
        }
      }
    }

    int radius = 160;
    int radiusSq = radius * radius;
    for (int z = -radius; z <= radius; ++z) {
      for (int x = -radius; x <= radius; ++x) {
        if (x*x + z*z <= radiusSq + 1) {
          Ui8 &cell = map[MapPos(x, MAP_HORIZON_Y, z).index];
          if (cell == CELL_EMPTY)
            cell = CELL_GRASS;
        }
      }
    }

    free(caveBuffer);
    free(bushesBuffer);
    free(treesBuffer);
    free(tempBuffer);
    if (neighbors2d) {
      free(neighbors2d);
      neighbors2d = NULL;
    }

    // plant bushes
  }
  mapCharacters = (int*)malloc(MAP_SIZE * MAP_SIZE * MAP_SIZE * sizeof(int));
  ClearMapCharacters();
}

void InitCharacters() {
  if (characters) {
    free(characters);
    characters = NULL;
  }

  characters = (Character*)malloc(MAX_CHARACTERS * sizeof(Character));

  nCharacters = 1;
  characters[0].Initialize();
  characters[0].type = CHARACTER_PLAYER;
  int yPos = MAP_HORIZON_Y + 1;
  for (; yPos < MAP_SIZE - 2; ++yPos) {
    if (map[MapPos(0, yPos, 0).index] == CELL_EMPTY && map[MapPos(0, yPos + 1, 0).index] == CELL_EMPTY)
      break;
  }
  characters[0].pos.Set(0, yPos, 0);


  AddCharacter(0);

  pLocalPlayer = &characters[0];

  int radius = 60;
  int radiusSq = radius*radius;

  for (int i = 0; i < MARTIANIAN_COUNT; ++i) {
    int x = (int)Random(0, MAP_SIZE_1);
    int z = (int)Random(0, MAP_SIZE_1);
    bool isOk = false;
    for (int y = 1; y <= MAP_HORIZON_Y + 1; y++) {
      MapPos mapPos(x, y, z);
      Ui8 cell = map[mapPos.index];
      if (cell == CELL_EMPTY && (Random(0, 6) == 0) && mapCharacters[mapPos.index] == -1) {
        if (y >= MAP_HORIZON_Y && x*x + y*y <= radiusSq)
          break;
        characters[nCharacters].Initialize();
        characters[nCharacters].hp = 3;
        characters[nCharacters].pos.Set(x, y, z);
        characters[nCharacters].type = CHARACTER_MARTIANIAN;
        AddCharacter(nCharacters);
        nCharacters++;
        isOk = true;
        break;
      }
    }
    if (!isOk)
      i--;
  }

  for (int i = 0; i < AMMO_COUNT; ++i) {
    int x = (int)Random(0, MAP_SIZE_1);
    int z = (int)Random(0, MAP_SIZE_1);
    bool isOk = false;
    for (int y = 1; y <= MAP_HORIZON_Y + 1; y++) {
      MapPos mapPos(x, y, z);
      Ui8 cell = map[mapPos.index];
      if (cell == CELL_EMPTY && (Random(0, 6) == 0) && mapCharacters[mapPos.index] == -1) {
        characters[nCharacters].Initialize();
        characters[nCharacters].hp = 1;
        characters[nCharacters].ammo = (int)Random(0, 2) ? 10 : 20;
        characters[nCharacters].pos.Set(x, y, z);
        characters[nCharacters].type = CHARACTER_AMMO;
        AddCharacter(nCharacters);
        nCharacters++;
        isOk = true;
        break;
      }
    }
    if (!isOk)
      i--;
  }
}

void MatchInit() {
  PlayerInit();
  InitMap();
  InitCharacters();
}

void DrawFrags() {
  char info[1024];
  float fps;
  static float prevFps[10] = {0,0,0,0,0,0,0,0,0,0};
  static int pervFpsIndex = 0;

  fps = (float)(1.0f / (curTime - prevTime + 0.001));

  prevFps[pervFpsIndex] = fps;
  pervFpsIndex = (pervFpsIndex + 1) % 10;

  fps = 0;
  for (int i = 0; i<10; i++)
    fps += prevFps[i];
  fps *= 0.1f;

  sprintf(info, "LEVEL:%d  HP:%d  FRAGS:%d  AMMO:%d  FPS:%0.01f",
    pLocalPlayer ? MAP_HORIZON_Y - pLocalPlayer->pos.y + 1 : 0,
    pLocalPlayer ? pLocalPlayer->hp : 100,
    frags,
    pLocalPlayer ? pLocalPlayer->ammo : 100,
    fps);

  DrawString(0, scrHeight_1, info, COL_0101, COL_0000);
}



void FixedFpsUpdateTime() {
  prevTime = curTime;
  curTime = Time();
  timeLapsed = (float)(curTime - prevTime);

  const float desiredFps = 24.f;
  double toSleep = (1.f / desiredFps - timeLapsed) - 0.001;
  if (toSleep > 0.003) {
    Sleep(toSleep);
  }
  curTime = Time();
  timeLapsed = (float)(curTime - prevTime);
}

void ShowMenu(bool isIngame) {
  float timeToAccept = 0.1f;
  int frameNo = 0;
  int currentMenuItem = isIngame && pLocalPlayer && pLocalPlayer->hp > 0 ? 1 : 0;
  while (true) {
    frameNo++;
    cls();

    const char* title = " M A R T I A N   D E E P   3";
    DrawString((scrWidth - (int)strlen(title)) / 2,
      scrHeight / 4 - (frameNo % 193 == 0 ? 1 : 0),
      title, COL_1001, COL_0000);
    if (frameNo % 193 == 0) {
      DrawString((scrWidth - (int)strlen(title)) / 8,
        scrHeight / 2,
        title, COL_1001, COL_0000);
    }

    int y = scrHeight / 2 - 1;

    const char *menu[] = {"NEW GAME", "CONTINUE", "EXIT"};
    unsigned int menuItems = sizeof(menu) / sizeof(menu[0]);
    for (unsigned int i = 0; i < menuItems; ++i) {
      int color = (currentMenuItem == i ? (frameNo / 8 % 2 == 0 ? COL_1111 : COL_0011) : COL_0010);
      DrawString((scrWidth - (int)strlen(menu[i])) / 2, y++, menu[i], color, COL_0000);
    }

    const char* controlsHelp = "INGAME CONTROLS: CTRL, SPACE, ARROW KEYS or X, Z, W, S, A, D";
    DrawString((scrWidth - (int)strlen(controlsHelp)) / 2,
      scrHeight * 3 / 4 - (frameNo % 193 == 0 ? 1 : 0),
      controlsHelp, COL_1000, COL_0000);

    present();
    FixedFpsUpdateTime();

    timeToAccept -= timeLapsed;
    if ((IsKeyDown(kKeyDown) || IsKeyDown(kKeyS)) && timeToAccept <= 0.f) {
      currentMenuItem = (currentMenuItem + 1) % menuItems;
      timeToAccept = 0.1f;
      sound_click.Play(0.01f * Random(85, 115));
    }
    if ((IsKeyDown(kKeyUp) || IsKeyDown(kKeyW)) && timeToAccept <= 0.f) {
      currentMenuItem = (menuItems + currentMenuItem - 1) % menuItems;
      timeToAccept = 0.1f;
      sound_click.Play(0.01f * Random(85, 115));
    }

    if ((IsKeyDown(kKeyEnter) || IsKeyDown(kKeySpace) || IsKeyDown(kKeyControl) || IsKeyDown(kKeyZ) || IsKeyDown(kKeyX)) && timeToAccept <= 0.f) {
      sound_menu_choise.Play(0.01f * Random(85, 115));
      if (currentMenuItem == 0) {
        prevTime = Time();
        curTime = Time();
        timeLapsed = 0;

        if (isIngame)
          MatchInit();
        return;
      }

      if (currentMenuItem == 1) {
        prevTime = Time();
        curTime = Time();
        timeLapsed = 0;
        return;
      }

      if (currentMenuItem == menuItems - 1) {
        Sleep(0.350);
        ExitProgram();
        return;
      }
    }

    if (IsKeyDown(kKeyEscape) && timeToAccept <= 0.f) {
      timeToAccept = 0.1f;
      if (currentMenuItem == menuItems - 1) {
        ExitProgram();;
        return;
      }
      currentMenuItem = menuItems - 1;
    }

    UpdateMusicPlayer();
  }
}
void ProcessKeyboard() {
  bool state;

  state = IsKeyDown(kKeyEscape);
  if (state) {
    ShowMenu(true);
  };

  if (IsKeyDown(kKeyQ)) {
    if (pLocalPlayer)
      pLocalPlayer = NULL;
    else
      pLocalPlayer = &characters[0];
    Sleep(0.300);
  }

  bool keyUp = IsKeyDown(kKeyR) || IsKeyDown(kKeySpace) || IsKeyDown(kKeyZ);
  bool keyDown = IsKeyDown(kKeyF);
  bool keyForward = IsKeyDown(kKeyUp) || IsKeyDown(kKeyW);
  bool keyBack = IsKeyDown(kKeyDown) || IsKeyDown(kKeyS);
  bool keyLeft = IsKeyDown(kKeyLeft) || IsKeyDown(kKeyA);
  bool keyRight = IsKeyDown(kKeyRight) || IsKeyDown(kKeyD);
  bool keyShoot = IsKeyDown(kKeyControl) || IsKeyDown(kKeyX);

  IPoint3d direction(0, 0, 0);
  if (pLocalPlayer && pLocalPlayer->timeToMove != MOVEMENT_TICKS)
    direction = pLocalPlayer->controlDirection;
  if (keyUp)
    direction.y += 1;
  if (keyDown)
    direction.y -= 1;
  if (keyForward)
    direction.z -= 1;
  if (keyBack)
    direction.z += 1;
  if (keyLeft)
    direction.x -= 1;
  if (keyRight)
    direction.x += 1;

  direction.x = std::min(1, std::max(-1, direction.x));
  direction.y = std::min(1, std::max(-1, direction.y));
  direction.z = std::min(1, std::max(-1, direction.z));

  if (pLocalPlayer) {
    pLocalPlayer->controlDirection = direction;
    if (direction.x != 0 || direction.y != 0 || direction.z != 0)
      pLocalPlayer->lastControlDirection = direction;
    if (keyShoot && pLocalPlayer->timeToShoot == 0) {
      pLocalPlayer->isShooting = true;
    }
  } else {
    IPoint3d iCameraPos = cameraPos.getPos();
    iCameraPos = iCameraPos + direction;
    cameraPos.setXyzSafe(iCameraPos);

    if (IsKeyDown(kKey1)) {
      map[cameraPos.index] = CELL_EMPTY;
      if (isInTunnelingMode) {
        for (int z = -1; z < 2; z++)
          for (int y = -1; y < 2; y++)
            for (int x = -1; x < 2; x++) {
              MapPos wallPos(cameraPos.getX() + x, cameraPos.getY() + y, cameraPos.getZ() + z);
              Ui8 cell = map[wallPos.index];
              if (cell != CELL_EMPTY)
                map[wallPos.index] = CELL_STONE;
            }

      }
    }
    if (IsKeyDown(kKey5)) {
      for (int i = 0; i < 2; ++i) {
        IPoint3d pos = cameraPos.getPos();
        pos.y += i;
        MapPos mapPos(pos);

        map[mapPos.index] = CELL_EMPTY;
        if (isInTunnelingMode) {
          for (int z = -1; z < 2; z++)
            for (int y = -1; y < 2; y++)
              for (int x = -1; x < 2; x++) {
                MapPos wallPos(mapPos.getX() + x, mapPos.getY() + y, mapPos.getZ() + z);
                Ui8 cell = map[wallPos.index];
                if (cell != CELL_EMPTY)
                  map[wallPos.index] = CELL_STONE;
              }

        }
      }
    }
    if (IsKeyDown(kKey2))
      map[cameraPos.index] = CELL_GRASS;
    if (IsKeyDown(kKey3))
      map[cameraPos.index] = CELL_EARTH;
    if (IsKeyDown(kKey4))
      map[cameraPos.index] = CELL_STONE;
    if (IsKeyDown(kKeyT))
      isInTunnelingMode = !isInTunnelingMode;
  }
}

void DrawProjection(int screenX, int screenY, int wid, int hei, MapPos camPos, IPoint3d &dirX, IPoint3d &dirY, IPoint3d &dirDepthStep) {
  IPoint3d dirDepth(-dirDepthStep.x, -dirDepthStep.y, -dirDepthStep.z);
  IPoint3d cam(camPos.getX(), camPos.getY(), camPos.getZ());

  int missingAxisIndex = 0;
  int xAxisIndex = 0;
  int yAxisIndex = 0;
  int upAxisNewIndex = 0;
  for (int i = 0; i < 3; ++i) {
    if (dirX.component[i] == 0 && dirY.component[i] == 0) {
      missingAxisIndex = i;
    }
    if (dirX.component[i]) {
      xAxisIndex = i;
    }
    if (dirY.component[i]) {
      yAxisIndex = i;
    }
  }
  if (xAxisIndex == 1)
    upAxisNewIndex = 0;
  else if (yAxisIndex == 1)
    upAxisNewIndex = 1;
  else
    upAxisNewIndex = 3;

  IPoint3d minusHalfDirX((-dirX.x*wid) / 2, (-dirX.y*wid) / 2, (-dirX.z*wid) / 2);
  IPoint3d minusHalfDirY((-dirY.x*hei) / 2, (-dirY.y*hei) / 2, (-dirY.z*hei) / 2);
  IPoint3d m0 = cam + minusHalfDirX + minusHalfDirY;
  IPoint3d my = m0;

  for (int y = 0; y < hei; y++) {
    IPoint3d mPos = my;
    for (int x = 0; x < wid; x++) {
      int lastZ = 4;
      int color = COL_0000;

      IPoint3d mPos3d = mPos;
      for (int z = 0; z < 4; z++) {
        MapPos mapPos3d(mPos3d);
        Ui8 cell = map[mapPos3d.index];

        switch (cell) {
        case CELL_EARTH:
          color = z != 0 ? (z != 1 ? COL_0001 : COL_1000) : COL_1100;
          break;
        case CELL_GRASS:
          color = z ? COL_0100 : COL_0101;
          break;
        case CELL_STONE:
          color = z != 0 ? (z != 1 ? COL_0001 : COL_1110) : COL_1111;
          break;
        }

        if (mapCharacters[mapPos3d.index] >= 0) {
          Character &ch = characters[mapCharacters[mapPos3d.index]];
          color = z ? COL_1000 : COL_1001;
          switch (ch.type) {
          case CHARACTER_PLAYER:
            color = z ? COL_1010 : COL_1011;
            break;
          case CHARACTER_BODY:
            color = z ? COL_1000 : COL_1001;
            break;
          case CHARACTER_BULLET:
            color = z ? COL_1100 : COL_1101;
            break;
          case CHARACTER_MARTIANIAN:
            color = z ? COL_1000 : COL_1001;
            break;
          case CHARACTER_AMMO:
            color = z ? COL_0110 : COL_0111;
            break;
          }
        }

        mPos3d = mPos3d + dirDepthStep;
        if (color) {
          lastZ = z;
          break;
        }
      }

      if (missingAxisIndex != 1 && mPos3d.y >= MAP_HORIZON_Y && lastZ == 4) {
        lastZ = 10;
        for (int z = 4; z < 10; z++) {
          MapPos mapPos3d(mPos3d);
          Ui8 cell = map[mapPos3d.index];
          if (cell != CELL_EMPTY) {
            lastZ = z;
            break;
          }
          mPos3d = mPos3d + dirDepthStep;
        }
        if (lastZ == 10)
          color = COL_0011;
      }
      DrawSquare(x + screenX, y + screenY, color);

      mPos = mPos + dirX;
    }
    my = my + dirY;
  }

  if (!pLocalPlayer) {
    int x = screenX + wid / 2;
    int y = screenY + hei / 2;
    Symbol &s = text_buf[x + y*scrWidth];
    s.back_color = s.back_color;
    s.fore_color = Rgba(255 - s.back_color.r, 255 - s.back_color.g, 255 - s.back_color.b, 255);
    s.code = 'X';
  }

}

void ProcessGameObjects() {
  for (int i = 0; i < nCharacters; ++i) {
    Character &ch = characters[i];
    IPoint3d oldPos = ch.pos;

    if (ch.type == CHARACTER_DELETED) {
      WipeDeletedCharacter(i);
      i--;
      continue;
    }

    if (ch.type == CHARACTER_AMMO) {
      if (ch.hp <= 0) {
        sound_impact_wood.Play(0.01f * Random(85, 115));
        DeleteCharacter(i);
        continue;
      }
    }

    if (ch.type == CHARACTER_MARTIANIAN) {
      // Die
      if (ch.hp <= 0) {
        sound_monster[Random(0, sound_monster_count-1)].Play(0.01f * Random(85, 115));

        if (ch.isPlayerBullet) {
          frags++;
        }
        DeleteCharacter(i);
        continue;
      }

      if (pLocalPlayer) {
        IPoint3d dir = pLocalPlayer->pos - ch.pos;
        if (abs(dir.x) < 40 && abs(dir.y) < 40 && abs(dir.z) < 40) {
          for (int componentIndex = 0; componentIndex < 3; componentIndex++) {
            if (Random(0, 4) != 0) {
              ch.controlDirection.component[componentIndex] = std::min(1, std::max(-1, dir.component[componentIndex]));
            } else {
              ch.controlDirection.component[componentIndex] = (int)Random(0, 2) - 1;
            }
          }
        } else {
          for (int componentIndex = 0; componentIndex < 3; componentIndex++) {
            if (Random(0, 4) == 0) {
              ch.controlDirection.component[componentIndex] = (int)Random(0, 2) - 1;
            }
          }
        }
      }
    }

    if (ch.type == CHARACTER_BULLET) {
      IPoint3d newPos = oldPos + ch.controlDirection;

      Ui8 cell = map[MapPos(newPos).index];
      if (cell != CELL_EMPTY) {
        DeleteCharacter(i);
        continue;
      }
      int characterAtNewPosIndex = mapCharacters[MapPos(newPos).index];
      if (characterAtNewPosIndex != -1) {
        DeleteCharacter(i);
        characters[characterAtNewPosIndex].hp -= 5;
        continue;
      }

      if (ch.timeToLive <= 0) {
        DeleteCharacter(i);
        continue;
      } else {
        ch.timeToLive--;
      }

      MoveCharacter(i, newPos);
      continue;
    }

    // Gravity
    IPoint3d newPos = ch.pos + IPoint3d(0, ch.jumpUpLeft <= 0 ? -1 : 1, 0);
    Ui8 cell = map[MapPos(newPos).index];

    if (ch.ticksToGravity < 0) {
      if (cell == CELL_EMPTY && mapCharacters[MapPos(newPos).index] == -1) {
        MoveCharacter(i, newPos);
        ch.controlDirection.y = 0;
      }

      ch.ticksToGravity = GRAVITY_TICKS;

      if (ch.jumpUpLeft > 0)
        ch.jumpUpLeft--;
    } else {
      ch.ticksToGravity--;
    }

    IPoint3d underPos = ch.pos + IPoint3d(0, -1, 0);
    cell = map[MapPos(underPos).index];
    if (cell == CELL_EMPTY && mapCharacters[MapPos(underPos).index] == -1) {
      ch.controlDirection.y = 0;
    }

    if (ch.timeToMove < 0) {
      // Movement
      newPos = ch.pos + ch.controlDirection;
      Ui8 cell = map[MapPos(newPos).index];

      if (mapCharacters[MapPos(newPos).index] != -1) {
        int i2 = mapCharacters[MapPos(newPos).index];
        Check(i2 >= 0, "i2 < 0 !");

        Character &ch2 = characters[i2];
        if (ch.type == CHARACTER_PLAYER) {
          if (ch2.type == CHARACTER_AMMO) {
            sound_pick_item.Play(0.01f * Random(85, 115));
            sound_reload_weapon.Play(0.01f * Random(85, 115));
            ch.ammo += ch2.ammo;
            DeleteCharacter(i2);
          } else if (ch2.type == CHARACTER_MARTIANIAN) {
            ch2.hp--;
            sound_hit[Random(0, sound_hit_count - 1)].Play(0.01f * Random(85, 115));
          }
        }
        if (ch.type == CHARACTER_MARTIANIAN) {
          if (ch2.type == CHARACTER_PLAYER) {
            sound_pain[Random(0, sound_pain_count - 1)].Play(0.01f * Random(85, 115));
            ch2.hp -= (int)Random(0, 19);
            if (ch2.hp < 0)
              ch2.hp = 0;
          }
        }
      }

      if (cell == CELL_EMPTY && mapCharacters[MapPos(newPos).index] == -1) {
        MoveCharacter(i, newPos);
        if (ch.controlDirection.y > 0)
          ch.ticksToGravity = GRAVITY_TICKS;
        if (ch.type == CHARACTER_PLAYER && ch.controlDirection.y > 0)
          sound_jump[Random(0, sound_jump_count - 1)].Play(0.01f * Random(85, 115));
        if (ch.controlDirection.y > 0) {
          ch.jumpUpLeft = JUMP_UP_TICKS;
        }
      }

      if (ch.type == CHARACTER_PLAYER)
        ch.timeToMove = MOVEMENT_TICKS;
      else if (ch.type == CHARACTER_MARTIANIAN)
        ch.timeToMove = MARTIANIAN_TICKS;
    } else {
      ch.timeToMove--;
    }

    if (ch.timeToShoot <= 0) {
      if (ch.isShooting && !(ch.lastControlDirection.x == 0 && ch.lastControlDirection.y == 0 && ch.lastControlDirection.z == 0)) {
        IPoint3d bulletSpawnPos = ch.pos + ch.lastControlDirection;
        if (map[MapPos(bulletSpawnPos).index] == CELL_EMPTY && mapCharacters[MapPos(bulletSpawnPos).index] == -1) {
          ch.isShooting = false;

          if (ch.ammo > 0) {
            characters[nCharacters].Initialize();
            characters[nCharacters].type = CHARACTER_BULLET;
            characters[nCharacters].pos = bulletSpawnPos;
            characters[nCharacters].controlDirection = ch.lastControlDirection;
            AddCharacter(nCharacters);
            nCharacters++;
            ch.ammo--;

            sound_pistol[Random(0, sound_pistol_count - 1)].Play(0.01f * Random(85, 115));
          }

          ch.timeToShoot = SHOOTING_TICKS;
        }
      }
    } else {
      ch.timeToShoot--;
    }

    // Wrap
    ch.pos = MapPos(ch.pos).getPos();

  }
}

void GameCycle() {
  prevTime = Time();
  curTime = Time();
  timeLapsed = 0;

  while (true) {
    cls();

    if (pLocalPlayer) {
      cameraPos.setXyzSafe(pLocalPlayer->pos);
    }

    IPoint3d dirX(1, 0, 0);
    IPoint3d dirMinusX(-1, 0, 0);
    IPoint3d dirMinusY(0, -1, 0);
    IPoint3d dirY(0, 1, 0);
    IPoint3d dirZ(0, 0, 1);

    IPoint3d dirMinusZ(0, 0, -1);
    DrawProjection(0, 0, scrWidth / 2, scrHeight / 2, cameraPos, dirX, dirZ, dirMinusY);
    DrawProjection(0, scrHeight / 2, scrWidth / 2, scrHeight / 2, cameraPos, dirX, dirMinusY, dirMinusZ);
    DrawProjection(scrWidth / 2, 0, scrWidth / 2, scrHeight / 2, cameraPos, dirMinusY, dirZ, dirMinusX);
    if (pLocalPlayer)
      DrawProjection(scrWidth / 2, scrHeight / 2, scrWidth / 2, scrHeight / 2, cameraPos, dirX, dirZ, dirY);
    DrawFrags();

    if (pLocalPlayer) {
      if (pLocalPlayer->hp <= 0) {
        DrawString(scrWidth / 2 + 1, scrHeight / 2 + 1, "Y O U   A R E   D E A D .", COL_1001, COL_1000);
      }
      if (pLocalPlayer->pos.y <= 1) {
        DrawString(scrWidth / 2 + 1, scrHeight / 2 + 1, "V I C T O R Y !", COL_1001, COL_1000);
      }
    }

    present();

    FixedFpsUpdateTime();

    ProcessKeyboard();
    if (pLocalPlayer && pLocalPlayer->hp > 0) {
      ProcessGameObjects();
    }

    UpdateMusicPlayer();
  }
}

#pragma endregion

void EasyMain() {
  ResizeScreen(640, 400);
  SetFullScreen(true);
  SetVSync(true);

  // Load font
  Sprite font;
  font.Load("data/font_vga_rus.tga");
  g_ascii.resize(vec.size());
  for (Si32 idx = 0; idx < vec.size(); ++idx) {
    Si32 x = idx % 16;
    Si32 y = 15 - idx / 16;
    const Ui8* p = (const Ui8*)vec[idx].data();
    Utf32Reader reader;
    reader.Reset(p);
    Ui32 u = reader.ReadOne();
    Sprite tmp;
    tmp.Reference(font, x * 8, y * 16, 8, 16);
    g_ch[u].Clone(tmp);
    g_ch[u].UpdateOpaqueSpans();
    g_ascii[idx] = u;
  }

  HardInit();
  while (true) {
    ShowMenu(false);

    MatchInit();
    GameCycle();

    //ShowVictory();
  }
}
